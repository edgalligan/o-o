(() => {

const popup = document.createElement('div');
popup.setAttribute('class', 'popup hide');
popup.appendChild(document.createTextNode('copied'));
document.body.appendChild(popup);
const toast = () => {
    popup.classList.remove('hide'); console.log(popup.classList);
    setTimeout(() => {
        popup.classList.add('hide'); console.log(popup.classList);
    }, 10);
}

[...document.getElementsByTagName('a')].forEach(a => {
    const b = a.insertAdjacentElement('afterend', document.createElement('button'));
    b?.appendChild(document.createTextNode('📋'));
    b?.addEventListener('click', () => {
        navigator.clipboard.writeText(a.getAttribute('href'));
        toast();
    });
});

})();